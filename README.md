# hacia jardín celular 

colección de ejemplos y ejercicios de socket.io, enfocados a la interacción con dispositivos móviles.

campo de ideas y pruebas para la instalación interactiva "jardín celular"

## ejercicios/prototipos:

* __secuencia-colores__: las pantallas de los dispositivos cambian de color de acuerdo a una secuencia coordinada, como en "carrusel"
* __medio-las-traes__: todas las pantallas están en negro menos una en blanco, al tocar/clickear esa, se apaga y otra más se enciende
