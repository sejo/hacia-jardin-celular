# medio las traes

esta app hace que de los clientes conectados, solo uno tenga la pantalla blanca. al tocar/clickear esa pantalla, se apaga, y otra más se enciende.

desde el punto de vista de socket.io, hace uso de "rooms" y de mensajes cliente-servidor y servidor-clientes.

## para instalar y ejecutar

con los archivos descargados, y con `node` y `npm` instalados, hay que hacer lo siguiente en el directorio donde está este texto:

```
$ npm install
```

esto instalará las dependencias (`express` y `socket.io`) del proyecto.

luego para correr el servidor:

```
node app.js
```
