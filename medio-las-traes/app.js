let app = require('express')();
let http = require('http').Server(app);
let io = require('socket.io')(http);
let port = 3000;

// servidor básico
app.get('/', function(request, response){
	response.sendFile(__dirname+'/index.html');
});
http.listen(port, function(){
	console.log(`servidor en puerto ${port}`);
});

// setup "default" de socket.io + modificaciones
io.on('connection', function(socket){
	console.log(`new client: ${socket.id}`);
	//
	// añade al socket al room "off"
	socket.join('off', () => {
		// y si ya hay más de uno, inicia el "juego"
		if(io.nsps['/'].adapter.rooms["off"].length > 1){
			console.log("más de uno");
			notificaRandomClient();
		}
	});

	// número de clients conectads
	console.log( "conectads:", Object.keys(io.sockets.connected).length);


	// handler del evento de tipo "touch", enviado por les clients al tocar la pantalla
	socket.on('touch', function(data){
		// si el cliente en cuestión está en el grupo de "tag", entonces cambia la situación
		if( Object.keys(socket.rooms).includes('tag') ){
			console.log("toque válido recibido");
			notificaRandomClient();
		}

	});

	socket.on('disconnect', function () {
		console.log(`client disconnected: ${socket.id}`);
		console.log( "conectads:", Object.keys(io.sockets.connected).length);
	});
});


// *****************************************
// custom code!
// *****************************************

function notificaRandomClient(){
	// de los clients en el room "off", obtiene uno aleatoriamente
	let keys = Object.keys(io.nsps['/'].adapter.rooms['off'].sockets);
	let len = keys.length;
	let num = Math.floor( Math.random()*len );
	let id = keys[num];

	// cambia a quienes están en "tag" a "off"
	// (si es que hay alguien en el grupo)
	if(io.nsps['/'].adapter.rooms['tag']){
		tagkeys = Object.keys(io.nsps['/'].adapter.rooms['tag'].sockets);
		tagkeys.forEach( (item, index) => {
			io.sockets.connected[item].leave('tag');
			io.sockets.connected[item].join('off');
		});
	}

	// conecta el cliente obtenido aleatoriamente a "tag"
	// y desconectálo de off
	io.sockets.connected[id].leave('off');
	io.sockets.connected[id].join('tag');

	// imprime el estado de los rooms
//	console.log("taglen",io.nsps['/'].adapter.rooms['tag']);
//	console.log("offlen",io.nsps['/'].adapter.rooms['off']);

	// envía mensaje para actualizar pantallas
	io.in('off').emit('color','black');
	io.in('tag').emit('color','white');
}

