# secuencia de colores

esta app hace que los clientes conectados cambien su pantalla de color en
secuencia orquestada

## para instalar y ejecutar

con los archivos descargados, y con `node` y `npm` instalados, hay que hacer lo siguiente en el directorio donde está este texto:

```
$ npm install
```

esto instalará las dependencias (`express` y `socket.io`) del proyecto.

luego para correr el servidor:

```
node app.js
```

