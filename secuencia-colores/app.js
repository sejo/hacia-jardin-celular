let app = require('express')();
let http = require('http').Server(app);
let io = require('socket.io')(http);
let port = 3000;

// servidor básico
app.get('/', function(request, response){
	response.sendFile(__dirname+'/index.html');
});
http.listen(port, function(){
	console.log(`servidor en puerto ${port}`);
});

// setup "default" de socket.io
io.on('connection', function(socket){
	console.log(`new client: ${socket.id}`);
	socket.join('off');

	console.log( io.sockets.in('off').clients());
	printClientes();

	socket.on('disconnect', function () {
		console.log(`client disconnected: ${socket.id}`);
		printClientes();
	});
});

// imprime cuántos clientes hay conectades
function printClientes(){
	io.sockets.clients( function(error,clients) {
		console.log(`total clientes: ${clients.length}`); });
}

// *****************************************
// custom code!
// *****************************************
let intervalo = 800;
let colores = ['#ff00cc','#ffcc00','#00ffcc','#00ccff','#cc00ff','#ccff00'];
let pointer = 0;
let len = 0;
let color = 0;
let indice = 0;
// repite cada ${intervalo} ms
setInterval(function(){
	// obtén la lista de clientes actuales
	// io.sockets es el "default namespace"
	// https://socket.io/docs/server-api/#namespace-clients-callback
	io.sockets.clients( function(error, clientes){
		len = clientes.length;
		pointer = (pointer + 1) % len;
		// ...y a cada uno envía un mensaje (color) distinto de la secuencia
		clientes.forEach(function(item, index){
			indice = (index + pointer) % len % colores.length;
			color = colores[ indice ];
			io.to(item).emit('color',color);
//			console.log(item,index,indice,color);
		});
	});

}, intervalo);



