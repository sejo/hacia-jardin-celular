# teclado midi distribuido

esta app convierte a las pantallas touch en "teclas" de un sintetizador midi que se ejecuta en el servidor.

cada pantalla corresponde a una tecla/nota, que es enviada via midi desde el servidor a algún instrumento que la utilice.

en este caso hay un *patch* sencillo de puredata, `midi-synth.pd`, que puede servir como sintetizador que recibe las notas.

desde el punto de vista de socket.io, se usan mensajes cliente-servidor, y el servidor tiene el módulo de midi para enviar las notas.

## para instalar y ejecutar

con los archivos descargados, y con `node` y `npm` instalados, hay que hacer lo siguiente en el directorio donde está este texto:

```
$ npm install
```

esto instalará las dependencias (`express` y `socket.io`) del proyecto.

luego para correr el servidor:

```
node app.js
```
