let app = require('express')();
let http = require('http').Server(app);
let io = require('socket.io')(http);
let port = 3000;

const midi = require('midi');
const midioutput = new midi.Output();

// C4, D4, E4, G4, A4
// https://newt.phys.unsw.edu.au/jw/notes.html
let notas = [60, 62, 64, 67, 69];

// para midi:
//Count the available midioutput ports.
console.log("número de puertos midi ",midioutput.getPortCount());
//
// Get the name of a specified midioutput port.
console.log(midioutput.getPortName(0));
//
// Open the first available midioutput port.
midioutput.openPort(0);


function noteOn( nota ){
	// note on, canal 1
	// https://www.midi.org/specifications/item/table-1-summary-of-midi-message
	midioutput.sendMessage([144,nota,127]);
}

function noteOff( nota ){
	// canal 1
	midioutput.sendMessage([144,nota,0]);
}


// servidor básico
app.get('/', function(request, response){
	response.sendFile(__dirname+'/index.html');
});
http.listen(port, function(){
	console.log(`servidor en puerto ${port}`);
});

// setup "default" de socket.io + modificaciones
io.on('connection', function(socket){
	console.log(`new client: ${socket.id}`);

	// número de clients conectads
	console.log( "conectads:", Object.keys(io.sockets.connected).length);


	// handler del evento de tipo "touch" 
	// enviado por les clients al tocar la pantalla
	socket.on('touch', function(data){
		// obtiene el índice del client y úsalo
		// como índice de la nota a tocar/apagar
		let indice = Object.keys(io.sockets.connected).indexOf(socket.id);

		//console.log(data,indice,socket.id);

		if( data == 'on'){
			noteOn( notas[indice%notas.length] );
		}
		else if( data == 'off'){
			noteOff( notas[indice%notas.length] );
		}

	});

	socket.on('disconnect', function () {
		console.log(`client disconnected: ${socket.id}`);
		console.log( "conectads:", Object.keys(io.sockets.connected).length);
	});
});
